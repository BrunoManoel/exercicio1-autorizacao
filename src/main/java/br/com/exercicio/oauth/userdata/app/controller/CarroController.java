package br.com.exercicio.oauth.userdata.app.controller;

import br.com.exercicio.oauth.userdata.app.models.Carro;
import br.com.exercicio.oauth.userdata.app.security.Usuario;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CarroController {

    @GetMapping("/{modelo}")
    public Carro create(@PathVariable String modelo, @AuthenticationPrincipal Usuario usuario) {
        Carro carro = new Carro();
        carro.setModelo(modelo);
        carro.setDono(usuario.getName());
        return carro;
    }
}